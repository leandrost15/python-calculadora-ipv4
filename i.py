#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
#
## Author
# moinho
#
#  IP.py
# testado em (python3.x) somente

class _calc:
    @classmethod
    def ipv4(self,ip):
        def _toDecimal(self):
            _bin,_temp = 0,0

            for i in range(len(ip)):
                try:
                    _temp = _temp * 0xA + int(ip[i])
                except ValueError:
                    _bin <<= 0x8
                    _bin |= _temp
                    if ip[i] is "/": return _bin
                    else: _temp = 0

        def _maskToDecimal(self):
            _index = ip.index("/")
            return ( (0xFFFFFFFF) ^ (0xFFFFFFFF >> int(ip[_index+1:] )) )

        def _broadcastToDecimal(self):
            _index = ip.index("/")
            return 0xFFFFFFFF >> int(ip[_index+1:])

        _decBD = _broadcastToDecimal(self)
        _decIP = _toDecimal(self)
        _decMASK = _maskToDecimal(self)

        def _hosts(self):
            return (2 ** (32 - int(bin(_decMASK).count("1")))) - 2

        def _format(self,dec):
            _out,_cpy = "",dec
            for i in range(3):
                _out = "." + str(_cpy&0xFF) + _out
                _cpy >>= 0x8
            return str(_cpy&0xFF) + _out             
        
        def _visor(self):
            print()
            print("Rede: ", _format(self, _decIP&~_decBD))
            print("Broadcast/Range: ", _format(self, _decIP|_decBD))
            print("Mascara: ", _format(self,_decMASK))
            print("Hosts possiveis: ", _hosts(self))
            print()
        _visor(self)
        
IP = input("Me de o IP/MASK")
print()
_calc.ipv4(IP)
print()
